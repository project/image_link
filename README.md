CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Image Link is a field type that allows you to have one field that contains both
an image and a link.

This is perfect for badges, social media links, and more.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/image_link

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/image_link


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Image Link module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > [Content type to edit] and add a
       field.
    3. Choose Image Link fromm the drop down. Save field settings.
    4. Adjust any defaults. Save settings.

Now the content type has an image link field.
    1. Add an image.
    2. Add a destination. Start typing the title of a piece of content to select
       it. You can also enter an internal path such as /node/add or an external
       URL such as http://example.com. Enter <front> to link to the front page.
    3. Save.


MAINTAINERS
-----------

 * Adam Bergstein (nerdstein) - https://www.drupal.org/u/nerdstein

Supporting organization:

 * Hook 42 - https://www.drupal.org/hook-42
